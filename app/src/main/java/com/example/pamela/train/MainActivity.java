package com.example.pamela.train;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity  {
    public static boolean [] trainactiveArray ={false, false, false, false, false, false};
    public static int position;
    public static String trainNumber;
    String[] trainNumberArray= new String[6];
    public static boolean trainActive;
    private Handler[] handler = new Handler[6];
    Runnable runnable;
    private int delay[]= {0,0,0,0,0,0};
    private int polling[] = new int[6];
    private int foundTrain[] = new int[6];

    SharedPreferences sharedPreferences;
    String preferences[] ={"","","","","",""};
    String preferencesString = "";
    String preferencesString1 = "";

    String numberTrainPreferences[] = new String[6];
    int pollingPreferences[] = new int[6];


    String preferencesPoling[] ={"","","","","",""};


    public static ArrayList<TextView> textTrainArray = new ArrayList<>();
    private ArrayList<Button> buttonStopArray = new ArrayList<>();
    private ArrayList<Button> buttonEditArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences= getPreferences(MODE_PRIVATE);
        //String p = sharedPreferences.getString("numt", "");

        //String p ="";

        for (int i = 0 ; i<6; i++){
            String iString = String.valueOf(i);
            String numPol = sharedPreferences.getString("numpol"+iString, "");
            String numPolArray[] = numPol.split("/");
            if (numPolArray.length>1) {
                String numberTrain = numPolArray[0];
                String pollingTString = numPolArray[1];
                int pollingT = Integer.parseInt(pollingTString);
                numberTrainPreferences[i] = numberTrain;

                pollingPreferences[i] = pollingT;
            }else{
                String numberTrain = numPolArray[0];
                numberTrainPreferences[i] = numberTrain;
                pollingPreferences[i] = 10000;
            }
        }


        Button btnedit = (Button) findViewById(R.id.btnedit);
        Button btnedit1 = (Button) findViewById(R.id.btnedit1);
        Button btnedit2 = (Button) findViewById(R.id.btnedit2);
        Button btnedit3 = (Button) findViewById(R.id.btnedit3);
        Button btnedit4 = (Button) findViewById(R.id.btnedit4);
        Button btnedit5 = (Button) findViewById(R.id.btnedit5);
        this.buttonEditArray.add(btnedit);
        this.buttonEditArray.add(btnedit1);
        this.buttonEditArray.add(btnedit2);
        this.buttonEditArray.add(btnedit3);
        this.buttonEditArray.add(btnedit4);
        this.buttonEditArray.add(btnedit5);

        Button btnstop = (Button) findViewById(R.id.btnstop);
        Button btnstop1 = (Button) findViewById(R.id.btnstop1);
        Button btnstop2 = (Button) findViewById(R.id.btnstop2);
        Button btnstop3 = (Button) findViewById(R.id.btnstop3);
        Button btnstop4 = (Button) findViewById(R.id.btnstop4);
        Button btnstop5 = (Button) findViewById(R.id.btnstop5);
        this.buttonStopArray.add(btnstop);
        this.buttonStopArray.add(btnstop1);
        this.buttonStopArray.add(btnstop2);
        this.buttonStopArray.add(btnstop3);
        this.buttonStopArray.add(btnstop4);
        this.buttonStopArray.add(btnstop5);

        TextView textTrainInfo = (TextView) findViewById(R.id.traininfoview1);
        TextView textTrainInfo1 = (TextView) findViewById(R.id.traininfoview2);
        TextView textTrainInfo2 = (TextView) findViewById(R.id.traininfoview3);
        TextView textTrainInfo3 = (TextView) findViewById(R.id.traininfoview4);
        TextView textTrainInfo4 = (TextView) findViewById(R.id.traininfoview5);
        TextView textTrainInfo5 = (TextView) findViewById(R.id.traininfoview6);
        this.textTrainArray.add(textTrainInfo);
        this.textTrainArray.add(textTrainInfo1);
        this.textTrainArray.add(textTrainInfo2);
        this.textTrainArray.add(textTrainInfo3);
        this.textTrainArray.add(textTrainInfo4);
        this.textTrainArray.add(textTrainInfo5);


        if (numberTrainPreferences.length!=0){
            for (int i = 0; i< numberTrainPreferences.length; i++){
                int pol = pollingPreferences[i];
                polling[i]=10000;
                textTrainArray.get(i).setText("Train: "+numberTrainPreferences[i]);
                trainNumberArray[i]=numberTrainPreferences[i];
                handler[i]=new Handler();
            }
        }






        for (int i = 0; i < 6; i++) {
            final int arrayposition = i;
            foundTrain[i] = i;

            buttonEditArray.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder myBuild = new AlertDialog.Builder(MainActivity.this);
                    View addTrainView = getLayoutInflater().inflate(R.layout.add_train_layout, null);
                    myBuild.setView(addTrainView);
                    final AlertDialog adialog = myBuild.create();
                    adialog.show();
                    final EditText edit_train_number = addTrainView.findViewById(R.id.train_number_edit);
                    final EditText pollingEdit = addTrainView.findViewById(R.id.time_polling_edit);
                    Button btnadd = addTrainView.findViewById(R.id.btnadd);
                    Button btnexit = addTrainView.findViewById(R.id.btnexit);
                    btnadd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            trainNumber = edit_train_number.getText().toString();
                            String pollingString = pollingEdit.getText().toString();
                            polling[arrayposition] = Integer.parseInt(pollingString)*1000;
                            trainNumberArray[arrayposition]=trainNumber;
                            trainactiveArray[arrayposition] = true;
                            trainActive=true;
                            handler[arrayposition] = new Handler();

                            position = arrayposition;
                            Train train = new Train();
                            train.execute();
                            delay[arrayposition] = Train.timeDelay;
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            preferences[arrayposition] = trainNumber;
                            preferencesPoling[arrayposition] = String.valueOf(polling[arrayposition]);

                            String posString = String.valueOf(arrayposition);
                            editor.putString("numpol"+posString, /*preferencesString*/preferences[arrayposition]+"/"+preferencesPoling[arrayposition]);

                            editor.commit();

                            adialog.dismiss();
                            buttonStopArray.get(position).setText("Stop");
                            monitorTrain(position);




                        }
                    });
                    btnexit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            adialog.dismiss();
                        }
                    });
                }
            });



            buttonStopArray.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reset(arrayposition);

                }
            });




        }

    }




    private void reset (int arraypos){


        if(trainactiveArray[arraypos]){
            handler[arraypos].removeCallbacks(runnable);
            trainactiveArray[arraypos]=false;
            Toast.makeText(MainActivity.this, "Stop", Toast.LENGTH_LONG).show();

            buttonStopArray.get(arraypos).setText("Start");
        }else{
            trainactiveArray[arraypos]=true;

            monitorTrain(arraypos);
            Toast.makeText(MainActivity.this, "Start", Toast.LENGTH_LONG).show();
            buttonStopArray.get(arraypos).setText("Stop");

        }

    }

    public void notification(int p){
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new android.support.v4.app.NotificationCompat.Builder(MainActivity.this)
                        .setSmallIcon(R.drawable.notify)
                        .setContentTitle("Train "+ Train.numbert + " for " + Train.city)
                        .setContentText("Delay: "+Integer.valueOf(Train.timeDelay)+" minutes");
        Notification mNot= mBuilder.build();

        NotificationManager mNotMan = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotMan != null;
        mNotMan.notify(1,mNot);
    }

    private void monitorTrain(final int pos) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {




                if(trainactiveArray[pos]){

                    trainActive=trainactiveArray[pos];
                    position=foundTrain[pos];
                    trainNumber=trainNumberArray[pos];
                    Train train1 = new Train();
                    train1.execute();
                    int Vdelay = Train.timeDelay;

                    if(delay[pos]!=Vdelay){

                        delay[pos]=Vdelay;

                        notification(pos);

                    }

                    handler[pos].postDelayed(this, polling[pos]);

                }
            }



        });

    }









}

