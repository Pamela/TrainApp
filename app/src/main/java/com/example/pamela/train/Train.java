package com.example.pamela.train;

import android.graphics.Color;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by pamela on 07/03/2018.
 */

public class Train extends AsyncTask<String, Void, String> {

    public static String city;
    private boolean advace=false;
    private String trainInfo, trainSurvey;
    public static String numbert;
    private String colorLayout;
    public static int timeDelay;

    @Override
    protected String doInBackground(String... params) {


        if (MainActivity.trainactiveArray[MainActivity.position]) {
            try {

                URL url = new URL("http://mobile.viaggiatreno.it/vt_pax_internet/mobile/numero?numeroTreno=" + MainActivity.trainNumber);
                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
                String input;
                numbert=MainActivity.trainNumber;
                while ((input = br.readLine()) != null) {

                    if ((input.contains("arrivato")) || (input.contains("partito")) || (input.contains("orario")) || (input.contains("anticipo")) || (input.contains("ritardo"))) {
                        trainInfo = input;

                    } else if (input.contains("<h2>")) {
                        city = input;
                        city = city.replaceFirst("\\s*", "");
                        city = city.replaceAll("[<>h2/]+", "");
                    } else if (input.contains("Ultimo rilevamento a")) {

                        trainSurvey = input;
                        trainSurvey = trainSurvey.replaceFirst("\\s*", "");
                        trainSurvey = trainSurvey.replaceAll("Ultimo rilevamento a" + "alle ore", "");

                    }
                    if(!input.contains("ritardo")){
                        timeDelay=0;
                    }
                }
                if (trainInfo.contains("ancora partito")) {
                    colorLayout = "blue";
                } else if ((trainInfo.contains("arrivato")) || (trainInfo.contains("anticipo")) || (trainInfo.contains("orario"))) {
                    colorLayout = "green";
                    if (trainInfo.contains("anticipo")) {
                        advace = true;
                    }
                } else if (trainInfo.contains("ritardo")) {
                    String time = trainInfo.replaceAll("[^0-9]+", "");
                    timeDelay = Integer.parseInt(time);


                    if (timeDelay > 10) {
                        colorLayout = "red";
                    } else {
                        colorLayout = "yellow";
                    }
                }
                br.close();

            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        } else {
            return trainInfo;
        }

        return  trainInfo;
    }
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(colorLayout=="blue"){

            MainActivity.textTrainArray.get(MainActivity.position).setText("Train "+ MainActivity.trainNumber+"\n not yet leave.");

            MainActivity.textTrainArray.get(MainActivity.position).setBackgroundColor(Color.parseColor("#6495ed"));
        }else if(colorLayout=="green"){
            if(!advace) {

                MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + " arrived!");
            }else{
                if(trainSurvey!=null) {
                    MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + " arrived earlier\n" + trainSurvey);
                }else {MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + " arrived earlier");
                }
            }
            MainActivity.textTrainArray.get(MainActivity.position).setBackgroundColor(Color.parseColor("#00ff00"));
        }else if(colorLayout=="yellow"){
            if(trainSurvey!=null) {
                MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + "\n " + timeDelay + " minutes delay\n" + trainSurvey);
            }else{MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + "\n " + timeDelay + " minutes delay");
            }

            MainActivity.textTrainArray.get(MainActivity.position).setBackgroundColor(Color.parseColor("#ffff00"));
        }else if(colorLayout=="red"){
            if(trainSurvey!=null) {
                MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + "\n " + timeDelay + " minutes delay\n" + trainSurvey);
            }else{MainActivity.textTrainArray.get(MainActivity.position).setText("Train " + MainActivity.trainNumber + "\n " + timeDelay + " minutes delay");
            }

            MainActivity.textTrainArray.get(MainActivity.position).setBackgroundColor(Color.parseColor("#ff0000"));

        }

    }




}

